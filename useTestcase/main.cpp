#include <iostream>

bool isPowerOfTwo(int n) {
    /* Return if n is the power of 2. */

    /* Below is a line of correct code, but I disabled it to let you see
     * how to use command to do a lot of tests at once.
     */
    // if(n <= 0)  return false;
    return (n &= (n-1)) == 0;
}

int main()
{
    int n;
    std::cin >> n;
    if (isPowerOfTwo(n))
    {
        std::cout << "true";
    }
    else
    {
        std::cout << "false";
    }
    
    return 0;
}