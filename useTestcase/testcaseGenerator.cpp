#include <iostream>
#include <fstream>

int main()
{
    const char* testcases[][2] = {
        {"-2", "false"},
        {"0" , "false"},
        {"1" , "true" },
        {"2" , "true" },
        {"3" , "false"},
        {"4" , "true" },
        {"1024", "true"},
        {"1023", "false"},
        {"16384", "true"},
        {"8084", "false"}
    };

    for (int i = 0; i < sizeof(testcases) / sizeof(testcases[0]); i++)
    {
        {
            std::ofstream fout;
            fout.open(std::to_string(i+1) + ".in", std::ios::ate|std::ios::out);
            fout << testcases[i][0];
        }

        {
            std::ofstream fout;
            fout.open(std::to_string(i+1) + ".out", std::ios::ate|std::ios::out);
            fout << testcases[i][1];
        }

    }
    
    return 0;
}
